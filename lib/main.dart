import 'dart:developer';

import 'dart:async';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'components/form.dart';
import 'models/barcode.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
        title: 'Startup Name Generator',
        theme: ThemeData(
          primaryColor: Colors.green,
        ),
        home: MainWidget(),
    );
  }
}

class MainWidget extends StatefulWidget {
  @override
  _MainWidgetState createState() => _MainWidgetState();
}

class _MainWidgetState extends State<MainWidget> {
  final _barCodes = <BarCode>[];
  final _favorites = Set<BarCode>();
  final _biggerFont = TextStyle(fontSize: 18.0);
  final _controller = TextEditingController();

  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    Future <void> _pushNew() async {
      _controller.clear();

      final text = await Navigator.of(context).push(
        MaterialPageRoute<void>(
          builder: (BuildContext context) {
            return Scaffold(
              appBar: AppBar(
                title: Text(_controller.text),
              ),
              body: MyCustomForm(controller: _controller)
            );
          },
        ),
      ) as String;

      if (text == null) { // no text returned
        return;
      }
      log("New bar code: $text");
      await addNewBarCodeToList(BarCode.fromCode(text));
    }

    void _pushSaved() {
      Navigator.of(context).push(
        MaterialPageRoute<void>(
          builder: (BuildContext context) {
            final tiles = _favorites.map(
              (BarCode barcode) {
                return ListTile(
                  title: Text(
                    barcode.code,
                    style: _biggerFont,
                  ),
                );
              },
            );
            final divided = ListTile.divideTiles(
              context: context,
              tiles: tiles,
            ).toList();

            return Scaffold(
              appBar: AppBar(
                title: Text('Favorite'),
              ),
              body: ListView(children: divided),
            );
          },
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Bar codes'),
        actions: [
          IconButton(icon: Icon(Icons.favorite), onPressed: _pushSaved),
          IconButton(icon: Icon(Icons.add), onPressed: () async {
            await _pushNew();
            log("Pressed ok");
            log("Codes ${_barCodes.length}");
          })
        ],
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: ListView.builder(
              padding: EdgeInsets.all(16.0),
              itemCount: _barCodes.length,
              itemBuilder: (context, i) {
                log("i: $i");
                log("Code: $i → ${_barCodes[i]}");
                return _buildRow(_barCodes[i]);
              }
            )
          )
        ]
      )
    );
  }

  Future <void> addNewBarCodeToList(BarCode item) async {
    setState(() { // Notify the framework that the internal state of this object has changed.
      _barCodes.insert(0, item);
    });

    // Save to sqlite db
    WidgetsFlutterBinding.ensureInitialized();
    // Open the database and store the reference.
    final Future<Database> database = openDatabase(
      // Set the path to the database. Note: Using the `join` function from the
      // `path` package is best practice to ensure the path is correctly
      // constructed for each platform.
      join(await getDatabasesPath(), 'barcodes_database.db'),
      // When the database is first created, create a table to store barcodes.
      onCreate: (db, version) {
        // Run the CREATE TABLE statement on the database.
        return db.execute(
          "CREATE TABLE barcodes(id INTEGER PRIMARY KEY, code TEXT)",
        );
      },
      // Set the version. This executes the onCreate function and provides a
      // path to perform database upgrades and downgrades.
      version: 1,
    );

    // Define a function that inserts barcodes into the database
    Future<void> insertBarCode(BarCode barCode) async {
      // Get a reference to the database.
      final Database db = await database;

      // Insert the BarCode into the correct table. You might also specify the
      // `conflictAlgorithm` to use in case the same barCode is inserted twice.
      //
      // In this case, replace any previous data.
      await db.insert(
        'barcodes',
        barCode.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace,
      );
    }

    // Create a BarCode and add it to the barcodes table.
    await insertBarCode(item);
  }

  Widget _buildRow(BarCode barCode) {
    final alreadySaved = _favorites.contains(barCode);
    return Card(
      child: ListTile(
        title: Text(
          barCode.code,
          style: _biggerFont,
        ),
        trailing: Icon(
          alreadySaved ? Icons.favorite : Icons.favorite_border,
          color: alreadySaved ? Colors.red : null,
        ),
        onTap: () {
          setState(() {
            if (alreadySaved) {
              _favorites.remove(barCode);
            } else {
              _favorites.add(barCode);
            }
          });
        },
      ),
    );
  }
}
