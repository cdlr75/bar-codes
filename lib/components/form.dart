import 'package:flutter/material.dart';

// Create a Form widget.
class MyCustomForm extends StatefulWidget {

  final TextEditingController controller;

  const MyCustomForm({Key key, this.controller}) : super(key: key);

  @override
  MyCustomFormState createState() {
    return MyCustomFormState(controller);
  }
}

// Create a corresponding State class.
// This class holds data related to the form.
class MyCustomFormState extends State<MyCustomForm> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a GlobalKey<FormState>,
  // not a GlobalKey<MyCustomFormState>.
  final _formKey = GlobalKey<FormState>();
  final TextEditingController controller;

  MyCustomFormState(this.controller);

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextFormField(
            controller: controller,
            onSaved: (String value) {
              // This optional block of code can be used to run
              // code when the user saves the form.
            },
            validator: (value) {
              if (value.isEmpty) {
                return 'Please enter some text';
              }
              return null;
            },
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: ElevatedButton(
              onPressed: () {
                // Validate returns true if the form is valid, or false
                // otherwise.
                if (_formKey.currentState.validate()) {
                  var text = controller.text;
                  // If the form is valid, display a Snackbar.
                  Scaffold.of(context)
                      // ignore: deprecated_member_use
                      .showSnackBar(SnackBar(content: Text('Processing Data $text')));
                  Navigator.pop(context, text); // data back to the first screen,
                }
              },
              child: Text('Submit'),
            ),
          ),
        ],
      ),
    );
  }
}
