
class BarCode {
  final int id;
  final String code;

  BarCode({this.id, this.code});

  BarCode.fromCode(this.code):
    // There's a better way to do this, stay tuned.
    this.id = DateTime.now().millisecondsSinceEpoch;

  // Convert a BarCode into a Map. The keys must correspond to the names of the
  // columns in the database.
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'code': code
    };
  }

  // Implement toString to make it easier to see information about
  // each barCode when using the print statement.
  @override
  String toString() {
    return 'BarCode{id: $id, code: $code}';
  }
}
